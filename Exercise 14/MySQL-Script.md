# Note:
Pada latihan ini akan menggunakan sample database "w3schools" di playground kelas praktikum A.

## MySQL UNION
MySQL UNION digunakan untuk menggabungkan hasil dari dua atau lebih pernyataan SELECT yang menghasilkan jumlah kolom, tipe data, dan urutan kolom yang sama. UNION menghapus duplikat secara otomatis. Namun, jika Anda ingin menyertakan duplikat, Anda dapat menggunakan UNION ALL.
### Contoh:
```sql
SELECT City FROM customers
UNION
SELECT City FROM suppliers
ORDER BY City;
```
```sql
SELECT City FROM customers
UNION ALL
SELECT City FROM suppliers
ORDER BY City;
```

## MySQL GROUP BY
MySQL GROUP BY digunakan untuk mengelompokkan baris berdasarkan nilai tertentu pada satu atau beberapa kolom. Dengan GROUP BY, Anda dapat mengaplikasikan fungsi agregat seperti COUNT, SUM, AVG, MAX, MIN, dll. pada setiap grup.
### Contoh:
```sql
SELECT City, COUNT(*) as jumlah_customers
FROM customers
GROUP BY City;
```

## MySQL HAVING
MySQL HAVING digunakan untuk menerapkan kondisi pada hasil pengelompokan yang dihasilkan oleh GROUP BY. HAVING berfungsi mirip dengan WHERE, tetapi HAVING digunakan setelah GROUP BY. Klausa HAVING ditambahkan ke SQL karena kata kunci WHERE tidak dapat digunakan dengan fungsi agregat.
### Contoh:
```sql
SELECT COUNT(CustomerID), Country
FROM customers
GROUP BY Country
HAVING COUNT(CustomerID) > 5;
```

## MySQL EXIST
MySQL EXISTS digunakan untuk memeriksa apakah subquery menghasilkan setidaknya satu baris. Jika subquery menghasilkan setidaknya satu baris, kondisi EXISTS akan bernilai TRUE.
### Contoh:
```sql
SELECT SupplierName
FROM suppliers
WHERE EXISTS (SELECT ProductName FROM products WHERE products.SupplierID = suppliers.supplierID AND Price = 22);
```

## MySQL ANY ALL
MySQL ANY dan ALL digunakan untuk membandingkan nilai dengan subquery. ANY digunakan ketika kondisi harus benar untuk setidaknya satu baris yang dipilih oleh subquery. ALL digunakan ketika kondisi harus benar untuk semua baris yang dipilih oleh subquery.
### Contoh:
```sql
SELECT ProductName
FROM products
WHERE ProductID = ANY
  (SELECT ProductID
  FROM order_details
  WHERE Quantity = 10);

```
```sql
SELECT ProductName
FROM products
WHERE ProductID = ALL
  (SELECT ProductID
  FROM order_details
  WHERE Quantity = 10);

```

## MySQL CASE
MySQL CASE digunakan untuk melakukan pengujian kondisi di dalam query. CASE memiliki dua bentuk, yaitu simple CASE dan searched CASE. Simple CASE membandingkan satu ekspresi dengan beberapa nilai, sedangkan searched CASE membandingkan beberapa kondisi.
### Contoh:
```sql
SELECT ProductName, Price,
   CASE
	  WHEN Price IS NULL THEN 'Ga ada harganya'
      WHEN Price < 10 THEN 'Murah'
      WHEN Price >= 10 THEN 'Mayan'
      WHEN Price >= 18 AND Price < 30 THEN 'Mahal'
      ELSE 'Mahal Bangetttt'
   END AS kategori_harga
FROM products ORDER BY Price;
```

## MySQL NULL FUNCTION
MySQL Null Function digunakan untuk memeriksa apakah nilai dalam kolom adalah NULL atau bukan. Beberapa fungsi NULL yang umum digunakan adalah IS NULL dan IS NOT NULL.
### Contoh:
```sql
SELECT CustomerName
FROM customers
WHERE City IS NULL;
```
```sql
SELECT CustomerName
FROM customers
WHERE City IS NOT NULL GROUP BY CustomerName;
```
## MySQL COMMENTS
MySQL Comments digunakan untuk memberikan komentar di dalam kode SQL. Ada dua jenis komentar yang umum digunakan, yaitu komentar satu baris (--) dan komentar multi-baris (/* */).
### Contoh:
```sql
SELECT City -- Ini adalah komentar satu baris
FROM customers;
```
```sql
/* Ini adalah komentar multi-baris
yang meliputi beberapa baris kode SQL */
SELECT City
FROM customers;

```

## MySQL OPERATORS
MySQL Operators digunakan untuk melakukan operasi matematika, perbandingan, logika, dan manipulasi string dalam query SQL. Beberapa operator umum yang digunakan adalah + (penjumlahan), - (pengurangan), * (perkalian), / (pembagian), = (sama dengan), > (lebih besar dari), < (lebih kecil dari), AND (logika AND), OR (logika OR), dll.
### Contoh:
```sql
SELECT ProductName, Price
FROM products
WHERE Price BETWEEN 10 AND 20;
```


