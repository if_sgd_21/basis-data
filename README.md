# Basis Data
Direktori ini berisi kumpulan tugas-tugas basis data yang telah dikerjakan. Tugas-tugas ini berkaitan dengan konsep, desain, implementasi, dan pengelolaan basis data.
## Daftar Tugas
<details>
<summary>Exercise 13</summary>
Mempelajari, mengeksekusi SQL script, dan menyelesaikan exercise mengenai:

- MySQL Aliases

- MySQL JOINS

- MySQL INNER JOIN

- MySQL LEFT JOIN

- MySQL RIGHT JOIN

- MySQL CROSS JOIN

- MySQL SELF JOIN

di w3schools.com:
https://www.w3schools.com/MYSQL/mysql_alias.asp
</details>
<details><summary>Exercise 14</summary>
mempelajari, mengeksekusi SQL script, dan menyelesaikan exercise mengenai:

- MySQL UNION

- MySQL GROUP BY

- MySQL HAVING

- MySQL EXISTS

- MySQL ANY ALL

- MySQL CASE

- MySQL Null Function

- MySQL Comments

- MySQL Operators

di w3schools.com:
https://www.w3schools.com/MySQL/mysql_union.asp
</details>

