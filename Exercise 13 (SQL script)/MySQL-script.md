# SQL SCRIPT

## MySQL Aliases
MySQL Aliases/Aliases digunakan untuk memberikan nama alternatif atau singkat untuk tabel atau kolom dalam sebuah query. Aliases berguna untuk membuat query lebih mudah dibaca dan mempermudah penggunaan kolom yang berulang atau nama tabel yang panjang. Alias ​​​​hanya ada selama durasi kueri itu. Sebuah alias dibuat dengan kata kunci **AS**.
### Contoh Script AS
```sql
SELECT CustomerID AS ID, CustomerName AS Customer
FROM Customers;
```
## MySQL Joins
MySQL JOINS digunakan untuk menggabungkan data dari dua atau lebih tabel berdasarkan kolom yang memiliki nilai yang sama. Berikut adalah demo tabel data dan beberapa jenis JOIN yang sering digunakan:
### Order Table
![Table Order](assets/img/OrderID-Table.png "Order ID")
### Customer Table
![Table Customer](assets/img/CustomerID-Table.png "Customer ID")
### MySQL INNER JOIN
![Ilustrasi INNER JOIN](assets/img/img_innerjoin.gif "Inner JOIN")

INNER JOIN mengembalikan hanya baris yang memiliki nilai yang cocok di kedua tabel yang dihubungkan.
#### Contoh:
```sql
SELECT customers.nama, orders.nomor_pesanan
FROM customers
INNER JOIN orders ON customers.id_customer = orders.id_customer;
```
### MySQL LEFT JOIN
![Ilustrasi LEFT JOIN](assets/img/left-join.png "LEFT JOIN")

LEFT JOIN mengembalikan semua baris dari tabel kiri dan baris yang cocok dari tabel kanan. Jika tidak ada nilai yang cocok di tabel kanan, akan dihasilkan NULL.
#### Contoh:
```sql
SELECT Customers.CustomerName, Orders.OrderID
FROM Customers
LEFT JOIN Orders ON Customers.CustomerID = Orders.CustomerID
ORDER BY Customers.CustomerName;
```
### MySQL RIGHT JOIN
![Ilustrasi RIGHT JOIN](assets/img/sql-right-join.png "RIGHT JOIN")

RIGHT JOIN mengembalikan semua baris dari tabel kanan dan baris yang cocok dari tabel kiri. Jika tidak ada nilai yang cocok di tabel kiri, akan dihasilkan NULL.
### Contoh:
```sql
SELECT Orders.OrderID, Employees.LastName, Employees.FirstName
FROM Orders
RIGHT JOIN Employees ON Orders.EmployeeID = Employees.EmployeeID
ORDER BY Orders.OrderID;
```
### MySQL CROSS JOIN
![Ilustrasi CROSS JOIN](assets/img/cross-join-schema.png "CROSS JOIN")

CROSS JOIN menghasilkan kombinasi semua baris dari kedua tabel tanpa mempertimbangkan kondisi yang dihubungkan. Hasilnya adalah jumlah baris pada tabel kiri dikalikan jumlah baris pada tabel kanan.
### Contoh:
```sql
SELECT Customers.CustomerName, Orders.OrderID
FROM Customers
CROSS JOIN Orders;
```
### MySQL SELF JOIN
![Ilustrasi SELF JOIN](assets/img/self-join-schema.png "SELF JOIN")

MySQL SELF JOIN adalah saat kita menggabungkan tabel dengan dirinya sendiri. Dalam SELF JOIN, kita menggunakan alias untuk membedakan dua atau lebih penampilan tabel yang sama dalam query.
### Contoh:
```sql
SELECT A.CustomerName AS CustomerName1, B.CustomerName AS CustomerName2, A.City
FROM Customers A, Customers B
WHERE A.CustomerID <> B.CustomerID
AND A.City = B.City
ORDER BY A.City;
```

## Sumber Referensi:
- [w3chools](https://www.w3schools.com/MYSQL/mysql_join_self.asp)
- [ChatGPT](https://chat.openai.com/)
- [Devart](https://www.devart.com/dbforge/sql/sqlcomplete/sql-join-statements.html#sql-cross-join)

